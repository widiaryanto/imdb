package com.widi.aryanto.sagara.mandiri.app

import com.widi.aryanto.sagara.mandiri.BuildConfig
import com.widi.aryanto.sagara.mandiri.components.base.app.NetworkConfig


class NetworkConfigs : NetworkConfig() {
    override fun baseUrl(): String {
        return BuildConfig.BASE_URL
    }

    override fun timeOut(): Long {
        return 30L
    }
}