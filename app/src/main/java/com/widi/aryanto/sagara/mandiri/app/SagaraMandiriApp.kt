package com.widi.aryanto.sagara.mandiri.app

import com.widi.aryanto.sagara.mandiri.components.base.app.AppInitializer
import com.widi.aryanto.sagara.mandiri.components.base.app.CoreApplication
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class SagaraMandiriApp : CoreApplication() {

    @Inject
    lateinit var initializer: AppInitializer

    override fun onCreate() {
        super.onCreate()
        initializer.init(this)
    }
}