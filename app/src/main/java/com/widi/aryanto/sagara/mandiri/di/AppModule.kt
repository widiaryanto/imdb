package com.widi.aryanto.sagara.mandiri.di

import android.content.Context
import com.widi.aryanto.sagara.mandiri.app.NetworkConfigs
import com.widi.aryanto.sagara.mandiri.app.SagaraMandiriApp
import com.widi.aryanto.sagara.mandiri.components.base.app.AppInitializer
import com.widi.aryanto.sagara.mandiri.components.base.app.AppInitializerImpl
import com.widi.aryanto.sagara.mandiri.components.base.app.NetworkConfig
import com.widi.aryanto.sagara.mandiri.components.base.app.TimberInitializer
import com.widi.aryanto.sagara.mandiri.components.pref.CacheManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Provides
    @Singleton
    fun provideApplication(): SagaraMandiriApp {
        return SagaraMandiriApp()
    }

    @Provides
    @Singleton
    fun provideNetworkConfig(): NetworkConfig {
        return NetworkConfigs()
    }

    @Provides
    @Singleton
    fun provideCacheManager(@ApplicationContext context: Context): CacheManager {
        return CacheManager(context)
    }

    @Provides
    @Singleton
    fun provideTimberInitializer(
        networkConfig: NetworkConfig
    ) = TimberInitializer()

    @Provides
    @Singleton
    fun provideAppInitializer(
        timberInitializer: TimberInitializer
    ): AppInitializer {
        return AppInitializerImpl(timberInitializer)
    }
}