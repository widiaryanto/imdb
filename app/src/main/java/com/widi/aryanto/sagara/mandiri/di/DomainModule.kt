package com.widi.aryanto.sagara.mandiri.di

import com.widi.aryanto.sagara.detail.di.MovieDetailModule
import com.widi.aryanto.sagara.movie.di.MovieModule
import com.widi.aryanto.sagara.settings.di.LanguageModule
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(
    includes = [
        MovieModule::class,
        MovieDetailModule::class,
        LanguageModule::class
    ]
)
@InstallIn(SingletonComponent::class)
class DomainModule