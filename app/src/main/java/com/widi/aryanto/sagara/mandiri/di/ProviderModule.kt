package com.widi.aryanto.sagara.mandiri.di

import android.content.Context
import com.widi.aryanto.sagara.mandiri.common.provider.LanguageProvider
import com.widi.aryanto.sagara.mandiri.common.provider.ResourceProvider
import com.widi.aryanto.sagara.mandiri.common.provider.ThemeProvider
import com.widi.aryanto.sagara.mandiri.components.pref.CacheManager
import com.widi.aryanto.sagara.mandiri.providers.AppLanguageProvider
import com.widi.aryanto.sagara.mandiri.providers.AppResourceProvider
import com.widi.aryanto.sagara.mandiri.providers.AppThemeProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ProviderModule {

    @Provides
    @Singleton
    fun provideThemeProvider(@ApplicationContext context: Context): ThemeProvider {
        return AppThemeProvider(context)
    }

    @Provides
    @Singleton
    fun provideAppResourceProvider(@ApplicationContext context: Context): ResourceProvider {
        return AppResourceProvider(context)
    }

    @Provides
    @Singleton
    fun provideAppLanguageProvider(cacheManager: CacheManager): LanguageProvider {
        return AppLanguageProvider(cacheManager)
    }
}