package com.widi.aryanto.sagara.mandiri.di

import android.content.Context
import com.widi.aryanto.sagara.detail.data.remote.service.MovieDetailService
import com.widi.aryanto.sagara.mandiri.components.base.app.NetworkConfig
import com.widi.aryanto.sagara.mandiri.components.network.createHttpLoggingInterceptor
import com.widi.aryanto.sagara.mandiri.components.network.createHttpRequestInterceptor
import com.widi.aryanto.sagara.mandiri.components.network.createMoshi
import com.widi.aryanto.sagara.mandiri.components.network.createOkHttpClient
import com.widi.aryanto.sagara.mandiri.components.network.createRetrofitWithMoshi
import com.widi.aryanto.sagara.mandiri.components.network.interceptor.HttpRequestInterceptor
import com.widi.aryanto.sagara.movie.data.remote.service.MovieService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

private const val BASE_URL = "base_url"

@Module
@InstallIn(SingletonComponent::class)
class RemoteModule {

    @Provides
    @Singleton
    @Named(value = BASE_URL)
    fun provideBaseUrl(networkConfig: NetworkConfig): String {
        return networkConfig.baseUrl()
    }

    @Provides
    @Singleton
    fun provideMoshi(): com.squareup.moshi.Moshi {
        return createMoshi()
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): okhttp3.logging.HttpLoggingInterceptor {
        return createHttpLoggingInterceptor()
    }

    @Provides
    @Singleton
    fun provideHttpRequestInterceptor(): HttpRequestInterceptor {
        return createHttpRequestInterceptor()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        @ApplicationContext context: Context,
        httpLoggingInterceptor: okhttp3.logging.HttpLoggingInterceptor,
        httpRequestInterceptor: HttpRequestInterceptor
    ): okhttp3.OkHttpClient {
        return createOkHttpClient(
            isCache = true,
            interceptors = arrayOf(
                httpLoggingInterceptor,
                httpRequestInterceptor
            ),
            context = context
        )
    }

    @Provides
    @Singleton
    fun provideMovieService(
        @Named(value = BASE_URL) baseUrl: String,
        okHttpClient: okhttp3.OkHttpClient,
        moshi: com.squareup.moshi.Moshi
    ) = createRetrofitWithMoshi<MovieService>(
        okHttpClient = okHttpClient,
        moshi = moshi,
        baseUrl = baseUrl
    )

    @Provides
    @Singleton
    fun provideMovieDetailService(
        @Named(value = BASE_URL) baseUrl: String,
        okHttpClient: okhttp3.OkHttpClient,
        moshi: com.squareup.moshi.Moshi
    ) = createRetrofitWithMoshi<MovieDetailService>(
        okHttpClient = okHttpClient,
        moshi = moshi,
        baseUrl = baseUrl
    )
}