package com.widi.aryanto.sagara.mandiri.di

import android.content.Context
import com.widi.aryanto.sagara.detail.data.remote.service.MovieDetailService
import com.widi.aryanto.sagara.detail.data.repository.MovieDetailRepository
import com.widi.aryanto.sagara.movie.data.remote.service.MovieService
import com.widi.aryanto.sagara.movie.data.repository.MovieRepository
import com.widi.aryanto.sagara.settings.data.repository.LanguageRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {
    @Singleton
    @Provides
    fun provideMovieRepository(
        service: MovieService,
    ) = MovieRepository(service)

    @Singleton
    @Provides
    fun provideMovieDetailRepository(
        service: MovieDetailService,
    ) = MovieDetailRepository(service)

    @Singleton
    @Provides
    fun provideLanguageRepository(
        @ApplicationContext context: Context
    ) = LanguageRepository(context)
}