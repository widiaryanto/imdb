package com.widi.aryanto.sagara.mandiri.navigation

import androidx.lifecycle.ViewModel
import com.widi.aryanto.sagara.mandiri.common.provider.ThemeProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val themeProvider: ThemeProvider) : ViewModel() {

    fun themeProvider() = themeProvider
}