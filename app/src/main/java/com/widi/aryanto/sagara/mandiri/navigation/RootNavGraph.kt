package com.widi.aryanto.sagara.mandiri.navigation

import com.ramcosta.composedestinations.spec.DestinationSpec
import com.ramcosta.composedestinations.spec.NavGraphSpec
import com.widi.aryanto.sagara.detail.detail.DetailNavGraph
import com.widi.aryanto.sagara.mandiri.home.HomeNavGraph
import com.widi.aryanto.sagara.movie.list.MovieNavGraph
import com.widi.aryanto.sagara.settings.SettingsNavGraph

object RootNavGraph : NavGraphSpec {
    override val route = "root"

    override val destinationsByRoute = emptyMap<String, DestinationSpec<*>>()

    override val startRoute = HomeNavGraph

    override val nestedNavGraphs = listOf(
        HomeNavGraph,
        SettingsNavGraph,
        MovieNavGraph,
        DetailNavGraph
    )
}