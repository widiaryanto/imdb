package com.widi.aryanto.sagara.mandiri.providers

import androidx.navigation.NavController
import com.ramcosta.composedestinations.navigation.navigateTo
import com.widi.aryanto.sagara.detail.detail.destinations.MovieDetailScreenDestination
import com.widi.aryanto.sagara.mandiri.common.provider.NavigationProvider
import com.widi.aryanto.sagara.movie.list.destinations.MovieScreenDestination
import com.widi.aryanto.sagara.settings.destinations.AboutScreenDestination
import com.widi.aryanto.sagara.settings.destinations.LanguageScreenDestination

class AppNavigationProvider constructor(
    private val navController: NavController
) : NavigationProvider {

    override fun openMovieDetail(id: Int, title: String) {
        navController.navigateTo(MovieDetailScreenDestination(id, title))
    }

    override fun openMovies(id: Int, title: String) {
        navController.navigateTo(MovieScreenDestination(id, title))
    }

    override fun openAppLanguage() {
        navController.navigateTo(LanguageScreenDestination)
    }

    override fun openAbout() {
        navController.navigateTo(AboutScreenDestination)
    }

    override fun navigateUp() {
        navController.navigateUp()
    }
}