package com.widi.aryanto.sagara.mandiri.providers

import android.content.Context
import com.widi.aryanto.sagara.mandiri.common.provider.ResourceProvider

class AppResourceProvider(private val context: Context) : ResourceProvider {
    override fun getString(id: Int): String {
        return context.getString(id)
    }
}