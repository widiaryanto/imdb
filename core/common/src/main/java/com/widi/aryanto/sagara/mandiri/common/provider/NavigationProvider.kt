package com.widi.aryanto.sagara.mandiri.common.provider

interface NavigationProvider {
    fun openMovieDetail(id: Int, title: String)
    fun openMovies(id: Int, title: String)
    fun openAppLanguage()
    fun openAbout()
    fun navigateUp()
}