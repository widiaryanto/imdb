package com.widi.aryanto.sagara.mandiri.common.provider

import androidx.annotation.StringRes

interface ResourceProvider {
    fun getString(@StringRes id: Int): String
}