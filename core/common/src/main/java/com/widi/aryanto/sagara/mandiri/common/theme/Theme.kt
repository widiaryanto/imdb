package com.widi.aryanto.sagara.mandiri.common.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.*
import androidx.compose.runtime.Composable

private val darkColorPalette = darkColorScheme(
    primary = Blue,
    onPrimary = White,
    secondary = Red,
    onSecondary = Black,

    background = BackgroundDark,
    onBackground = BackgroundDark,

    surface = CardDark,
    onSurface = CardDark
)

private val lightColorPalette = lightColorScheme(
    primary = White,
    onPrimary = Black,
    secondary = Red,
    onSecondary = Black,

    background = BackgroundLight,
    onBackground = BackgroundLight,

    surface = White,
    onSurface = White
)

val SagaraMandiriColors: ColorScheme
    @Composable get() = MaterialTheme.colorScheme

val SagaraMandiriShapes: Shapes
    @Composable get() = MaterialTheme.shapes

val SagaraMandiriTypography: Typography
    @Composable get() = MaterialTheme.typography

@Composable
fun SagaraMandiriTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        darkColorPalette
    } else {
        lightColorPalette
    }

    val typography = if (darkTheme) {
        DarkTypography
    } else {
        LightTypography
    }

    MaterialTheme(
        colorScheme = colors,
        typography = typography,
        shapes = Shapes,
        content = content
    )
}