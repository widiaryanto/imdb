package com.widi.aryanto.sagara.mandiri.components.base.app

interface AppInitializer {
    fun init(application: CoreApplication)
}