package com.widi.aryanto.sagara.mandiri.components.base.app

abstract class NetworkConfig {
    abstract fun baseUrl(): String

    abstract fun timeOut(): Long
}