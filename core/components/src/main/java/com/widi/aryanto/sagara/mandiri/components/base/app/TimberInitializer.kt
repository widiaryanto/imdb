package com.widi.aryanto.sagara.mandiri.components.base.app

import timber.log.Timber

class TimberInitializer : AppInitializer {
    override fun init(application: CoreApplication) {
        Timber.plant(Timber.DebugTree())
    }
}