package com.widi.aryanto.sagara.mandiri.components.network.environment

import androidx.annotation.IntDef
import com.widi.aryanto.sagara.mandiri.components.network.environment.Environment.Companion.DEVELOPMENT
import com.widi.aryanto.sagara.mandiri.components.network.environment.Environment.Companion.PRODUCTION
import com.widi.aryanto.sagara.mandiri.components.network.environment.Environment.Companion.STAGING

@IntDef(DEVELOPMENT, STAGING, PRODUCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class Environment {
    companion object {
        const val DEVELOPMENT = 1
        const val STAGING = 2
        const val PRODUCTION = 3
    }
}