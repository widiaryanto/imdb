package com.widi.aryanto.sagara.mandiri.components.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber

class HttpRequestInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val request = originalRequest
            .newBuilder()
            .url(originalRequest.url)
            .addHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2MGQwYzUwYmRjNDc0NDQxODRiY2Q2ZjBhY2RmYjEyZSIsInN1YiI6IjViMDFmYWNlMGUwYTI2MjNlOTAwZmNiYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.9uBi4P2aHAUa9BAsxmXUTfkUoqsUql7SFGhjubQejF0")
            .addHeader("accept", "application/json")
            .build()
        Timber.d(request.toString())
        return chain.proceed(request)
    }
}