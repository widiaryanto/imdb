package com.widi.aryanto.sagara.mandiri.components.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class DateUtils {
    companion object {
        const val FORMAT_DATE_FULL_LOCALE = "MMMM dd, yyyy"
        const val FORMAT_DATE_FULL_LOCALE_ID = "dd MMMM, yyyy"
        const val FORMAT_DATE_SIMPLE = "yyyy-MM-dd"
    }

    fun dateMovie(time: String?): String {
        val localeFormat: String = if (Locale.getDefault().toString().contains("in") ||
            Locale.getDefault().toString().contains("ID")
        ) {
            FORMAT_DATE_FULL_LOCALE_ID
        } else {
            FORMAT_DATE_FULL_LOCALE
        }
        return try {
            val dateFormatter = SimpleDateFormat(FORMAT_DATE_SIMPLE, Locale.getDefault())
            val date: Date? = time?.let { dateFormatter.parse(it) }
            val timeFormatter = SimpleDateFormat(localeFormat, Locale.getDefault())
            date?.let { timeFormatter.format(it) }.toString()
        } catch (e: ParseException) {
            ""
        }
    }
}