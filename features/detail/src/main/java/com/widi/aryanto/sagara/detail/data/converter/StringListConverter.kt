package com.widi.aryanto.sagara.detail.data.converter

import androidx.room.TypeConverter
import com.widi.aryanto.sagara.mandiri.components.extension.fromJson
import com.widi.aryanto.sagara.mandiri.components.extension.toJson

class StringListConverter {
    @TypeConverter
    fun toListOfStrings(stringValue: String): List<String>? {
        return stringValue.fromJson()
    }

    @TypeConverter
    fun fromListOfStrings(listOfString: List<String>?): String {
        return listOfString.toJson()
    }
}