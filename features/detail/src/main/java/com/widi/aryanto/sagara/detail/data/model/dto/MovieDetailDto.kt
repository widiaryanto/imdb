package com.widi.aryanto.sagara.detail.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetailDto(
    val adult: Boolean,
    val backdropPath: String,
    val budget: Int,
    val genres: List<MovieDetailGenre>,
    val homepage: String,
    val id: Int,
    val imdbId: String,
    val originalLanguage: String,
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    val posterPath: String,
    val productionCompanies: List<MovieDetailProduction>,
    val productionCountries: List<MovieDetailProductionCountry>,
    val releaseDate: String,
    val revenue: Int,
    val runtime: Int,
    val spokenLanguages: List<MovieDetailSpoken>,
    val status: String,
    val tagline: String,
    val title: String,
    val video: Boolean,
    val voteAverage: Double,
    val voteCount: Int,
    var youtube: MutableList<MovieDetailYoutube> = mutableListOf(),
    var reviews: MutableList<MovieDetailReviews> = mutableListOf()
) : Parcelable {
    companion object {
        fun init() = MovieDetailDto(
            adult = false,
            backdropPath = "/pA3vdhadJPxF5GA1uo8OPTiNQDT.jpg",
            budget = 15000000,
            genres = listOf(
                MovieDetailGenre(
                    id = 28,
                    name = "Action"
                ),
                MovieDetailGenre(
                    id = 18,
                    name = "Drama"
                ),
            ),
            homepage = "https://www.soundoffreedommovie.com/",
            id = 678512,
            imdbId = "tt7599146",
            originalLanguage = "en",
            originalTitle = "Sound of Freedom",
            overview = "The story of Tim Ballard, a former US government agent, who quits his job in order to devote his life to rescuing children from global sex traffickers.",
            popularity = 3187.754,
            posterPath = "/qA5kPYZA7FkVvqcEfJRoOy4kpHg.jpg",
            productionCompanies = listOf(
                MovieDetailProduction(
                    id = 90508,
                    logoPath = "",
                    name = "Santa Fe Films",
                    originCountry = "US"
                )
            ),
            productionCountries = listOf(
                MovieDetailProductionCountry(
                    iso31661 = "US",
                    name = "United States of America"
                )
            ),
            releaseDate = "2023-07-03",
            revenue = 230953510,
            runtime = 131,
            spokenLanguages = listOf(
                MovieDetailSpoken(
                    englishName = "English",
                    iso6391 = "en",
                    name = "English"
                )
            ),
            status = "Released",
            tagline = "Fight for the light. Silence the darkness.",
            title = "Sound of Freedom",
            video = false,
            voteAverage = 8.063,
            voteCount = 715,
            youtube = mutableListOf(
                MovieDetailYoutube.init()
            ),
            reviews = mutableListOf(
                MovieDetailReviews.init()
            )
        )
    }
}