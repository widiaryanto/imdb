package com.widi.aryanto.sagara.detail.data.model.dto

import com.widi.aryanto.sagara.mandiri.components.extension.orFalse
import com.widi.aryanto.sagara.mandiri.components.extension.orZero
import com.widi.aryanto.sagara.detail.data.remote.MovieDetailGenreResponse
import com.widi.aryanto.sagara.detail.data.remote.MovieDetailProductionCountryResponse
import com.widi.aryanto.sagara.detail.data.remote.MovieDetailProductionResponse
import com.widi.aryanto.sagara.detail.data.remote.MovieDetailResponse
import com.widi.aryanto.sagara.detail.data.remote.MovieDetailSpokenResponse

fun MovieDetailResponse.toMovieDetailDto() = MovieDetailDto(
    adult = adult.orFalse(),
    backdropPath = backdropPath.orEmpty(),
    budget = budget.orZero(),
    genres = genres?.toMovieGenreDetail() ?: emptyList(),
    homepage = homepage.orEmpty(),
    id = id.orZero(),
    imdbId = imdbId.orEmpty(),
    originalLanguage = originalLanguage.orEmpty(),
    originalTitle = originalTitle.orEmpty(),
    overview = overview.orEmpty(),
    popularity = popularity.orZero(),
    posterPath = posterPath.orEmpty(),
    productionCompanies = productionCompanies?.toMovieProductionDetail() ?: emptyList(),
    productionCountries = productionCountries?.toMovieProductionCountryDetail() ?: emptyList(),
    releaseDate = releaseDate.orEmpty(),
    revenue = revenue.orZero(),
    runtime = runtime.orZero(),
    spokenLanguages = spokenLanguages?.toMovieSpokenDetail() ?: emptyList(),
    status = status.orEmpty(),
    tagline = tagline.orEmpty(),
    title = title.orEmpty(),
    video = video.orFalse(),
    voteAverage = voteAverage.orZero(),
    voteCount = voteCount.orZero()
)

fun MovieDetailGenreResponse.toMovieDetailGenre() = MovieDetailGenre(
    id = id.orZero(),
    name = name.orEmpty()
)

fun MovieDetailProductionResponse.toMovieDetailProduction() = MovieDetailProduction(
    id = id.orZero(),
    logoPath = logoPath.orEmpty(),
    name = name.orEmpty(),
    originCountry = originCountry.orEmpty()
)

fun MovieDetailProductionCountryResponse.toMovieDetailProductionCountry() = MovieDetailProductionCountry(
    iso31661 = iso31661.orEmpty(),
    name = name.orEmpty()
)

fun MovieDetailSpokenResponse.toMovieDetailSpoken() = MovieDetailSpoken(
    englishName = englishName.orEmpty(),
    iso6391 = iso6391.orEmpty(),
    name = name.orEmpty()
)

fun List<MovieDetailGenreResponse>.toMovieGenreDetail() = map { it.toMovieDetailGenre() }

fun List<MovieDetailProductionResponse>.toMovieProductionDetail() = map { it.toMovieDetailProduction() }

fun List<MovieDetailProductionCountryResponse>.toMovieProductionCountryDetail() = map { it.toMovieDetailProductionCountry() }

fun List<MovieDetailSpokenResponse>.toMovieSpokenDetail() = map { it.toMovieDetailSpoken() }

fun List<MovieDetailResponse>.toMovieDtoDetail() = map { it.toMovieDetailDto() }