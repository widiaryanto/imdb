package com.widi.aryanto.sagara.detail.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetailProduction(
    val id: Int,
    val logoPath: String,
    val name: String,
    val originCountry: String
) : Parcelable