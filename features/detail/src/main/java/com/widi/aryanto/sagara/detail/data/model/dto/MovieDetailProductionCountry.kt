package com.widi.aryanto.sagara.detail.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetailProductionCountry(
    val iso31661: String,
    val name: String
) : Parcelable