package com.widi.aryanto.sagara.detail.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetailReviews(
    val author: String,
    val authorDetails: MovieDetailReviewsAuthor,
    val content: String,
    val createdAt: String,
    val id: String,
    val updatedAt: String,
    val url: String
) : Parcelable {
    companion object {
        fun init() = MovieDetailReviews(
            author = "EmmanuelGoldstein",
            authorDetails = MovieDetailReviewsAuthor.init(),
            content = "Theatrical Trailer for July 4",
            createdAt = "2023-05-12T02:03:12.000Z",
            id = "645e47e288b1480158f2e037",
            updatedAt = "2023-05-12T02:03:12.000Z",
            url = "https://www.themoviedb.org/review/64c30a506331b200ff0a64d7"
        )
    }
}