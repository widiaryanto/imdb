package com.widi.aryanto.sagara.detail.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetailReviewsAuthor(
    val name: String,
    val username: String,
    val avatarPath: String,
    val rating: Int
) : Parcelable {
    companion object {
        fun init() = MovieDetailReviewsAuthor(
            name = "",
            username = "EmmanuelGoldstein",
            avatarPath = "/4KVM1VkqmXLOuwj1jjaSdxbvBDk.jpg",
            rating = 6
        )
    }
}