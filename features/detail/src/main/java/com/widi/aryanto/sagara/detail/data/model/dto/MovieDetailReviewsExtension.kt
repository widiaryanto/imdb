package com.widi.aryanto.sagara.detail.data.model.dto

import com.widi.aryanto.sagara.detail.data.remote.MovieDetailReviewsData
import com.widi.aryanto.sagara.detail.data.remote.MovieDetailReviewsDataAuthor
import com.widi.aryanto.sagara.mandiri.components.extension.orZero

fun MovieDetailReviewsData.toMovieDetailReviews() = MovieDetailReviews(
    author = author.orEmpty(),
    authorDetails = authorDetails.toMovieDetailAuthor(),
    content = content.orEmpty(),
    createdAt = createdAt.orEmpty(),
    id = id.orEmpty(),
    updatedAt = updatedAt.orEmpty(),
    url = url.orEmpty()
)

fun MovieDetailReviewsDataAuthor.toMovieDetailAuthor() = MovieDetailReviewsAuthor(
    name = name.orEmpty(),
    username = username.orEmpty(),
    avatarPath = avatarPath.orEmpty(),
    rating = rating.orZero()
)

fun List<MovieDetailReviewsData>.toMovieReviewsDetail() = map { it.toMovieDetailReviews() }