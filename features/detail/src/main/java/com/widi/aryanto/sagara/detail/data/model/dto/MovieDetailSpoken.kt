package com.widi.aryanto.sagara.detail.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetailSpoken(
    val englishName: String,
    val iso6391: String,
    val name: String
) : Parcelable