package com.widi.aryanto.sagara.detail.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetailYoutube(
    val iso6391: String,
    val iso31661: String,
    val name: String,
    val key: String,
    val site: String,
    val size: Int,
    val type: String,
    val official: Boolean,
    val publishedAt: String,
    val id: String
) : Parcelable {
    companion object {
        fun init() = MovieDetailYoutube(
            iso6391 = "en",
            iso31661 = "US",
            name = "Theatrical Trailer for July 4",
            key = "hyyyKcfJRGQ",
            site = "YouTube",
            size = 2160,
            type = "Trailer",
            official = true,
            publishedAt = "2023-05-12T02:03:12.000Z",
            id = "645e47e288b1480158f2e037"
        )
    }
}