package com.widi.aryanto.sagara.detail.data.model.dto

import com.widi.aryanto.sagara.mandiri.components.extension.orFalse
import com.widi.aryanto.sagara.mandiri.components.extension.orZero
import com.widi.aryanto.sagara.detail.data.remote.MovieDetailYoutubeData

fun MovieDetailYoutubeData.toMovieDetailYoutube() = MovieDetailYoutube(
    iso6391 = iso6391.orEmpty(),
    iso31661 = iso31661.orEmpty(),
    name = name.orEmpty(),
    key = key.orEmpty(),
    site = site.orEmpty(),
    size = size.orZero(),
    type = type.orEmpty(),
    official = official.orFalse(),
    publishedAt = publishedAt.orEmpty(),
    id = id.orEmpty()
)

fun List<MovieDetailYoutubeData>.toMovieYoutubeDetail() = map { it.toMovieDetailYoutube() }