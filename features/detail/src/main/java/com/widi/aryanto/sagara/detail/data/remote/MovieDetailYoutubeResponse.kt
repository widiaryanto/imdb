package com.widi.aryanto.sagara.detail.data.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieDetailYoutubeResponse(
    @Json(name = "results") val results: List<MovieDetailYoutubeData>?
)