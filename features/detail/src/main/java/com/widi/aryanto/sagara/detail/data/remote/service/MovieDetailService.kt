package com.widi.aryanto.sagara.detail.data.remote.service

import com.widi.aryanto.sagara.detail.data.remote.MovieDetailResponse
import com.widi.aryanto.sagara.detail.data.remote.MovieDetailReviewsResponse
import com.widi.aryanto.sagara.detail.data.remote.MovieDetailYoutubeResponse
import retrofit2.http.*

interface MovieDetailService {

    @GET(MOVIES_DETAIL)
    suspend fun getMoviesDetail(
        @Path("movie_id") id: Int,
        @Query("language") language: String
    ): MovieDetailResponse

    @GET(MOVIES_YOUTUBE)
    suspend fun getMoviesDetailYoutube(
        @Path("movie_id") id: Int,
        @Query("language") language: String
    ): MovieDetailYoutubeResponse

    @GET(MOVIES_REVIEWS)
    suspend fun getMoviesDetailReviews(
        @Path("movie_id") id: Int,
        @Query("language") language: String,
        @Query("page") page: Int
    ): MovieDetailReviewsResponse

    companion object {
        const val MOVIES_DETAIL = "/3/movie/{movie_id}"
        const val MOVIES_YOUTUBE = "/3/movie/{movie_id}/videos"
        const val MOVIES_REVIEWS = "/3/movie/{movie_id}/reviews"
    }
}