package com.widi.aryanto.sagara.detail.data.repository

import com.widi.aryanto.sagara.detail.data.remote.service.MovieDetailService
import javax.inject.Inject

class MovieDetailRepository @Inject constructor(
    private val service: MovieDetailService
) {
    suspend fun getMovieDetail(
        id: Int,
        language: String
    ) = service.getMoviesDetail(id, language)

    suspend fun getMovieDetailYoutube(
        id: Int,
        language: String
    ) = service.getMoviesDetailYoutube(id, language)

    suspend fun getMovieDetailReview(
        id: Int,
        language: String,
        page: Int
    ) = service.getMoviesDetailReviews(id, language, page)
}