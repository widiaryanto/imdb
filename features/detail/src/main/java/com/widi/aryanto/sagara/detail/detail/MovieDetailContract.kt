package com.widi.aryanto.sagara.detail.detail

import com.widi.aryanto.sagara.detail.data.model.dto.MovieDetailDto

data class MovieDetailViewState(
    val movieDetail: MovieDetailDto? = null
)

sealed class MovieDetailEvent {
    data class LoadDetail(val id: Int, val language: String) : MovieDetailEvent()
}