package com.widi.aryanto.sagara.detail.detail

import android.content.res.Configuration
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.absoluteOffset
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.widi.aryanto.sagara.mandiri.common.component.widget.EmptyView
import com.widi.aryanto.sagara.mandiri.common.component.widget.ErrorView
import com.widi.aryanto.sagara.mandiri.common.component.widget.LoadingView
import com.widi.aryanto.sagara.mandiri.common.component.widget.SMToolbarWithNavIcon
import com.widi.aryanto.sagara.mandiri.common.provider.NavigationProvider
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriColors
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.mandiri.components.base.mvi.BaseViewState
import com.widi.aryanto.sagara.mandiri.components.extension.cast
import com.widi.aryanto.sagara.detail.detail.view.MovieDetailContent
import kotlinx.coroutines.delay

@Destination(start = true)
@Composable
fun MovieDetailScreen(
    id: Int = 0,
    title: String = "",
    viewModel: MovieDetailViewModel = hiltViewModel(),
    navigator: NavigationProvider
) {
    val uiState by viewModel.uiState.collectAsState()
    var movieDetailState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (movieDetailState) 400.dp else 0.dp,
        tween(1000), label = ""
    )

    LaunchedEffect(Unit) {
        delay(400)
        movieDetailState = false
    }

    MovieDetailBody(
        title = title,
        pressOnBack = { navigator.navigateUp() }
    ) { padding ->
        when (uiState) {
            is BaseViewState.Data -> MovieDetailContent(
                paddingValues = padding,
                viewState = uiState.cast<BaseViewState.Data<MovieDetailViewState>>().value,
            )
            is BaseViewState.Empty -> EmptyView(
                modifier = Modifier
                    .fillMaxSize()
                    .absoluteOffset(x = offsetAnimation)
            )
            is BaseViewState.Error -> ErrorView(
                modifier = Modifier.absoluteOffset(x = offsetAnimation),
                e = uiState.cast<BaseViewState.Error>().throwable,
                action = {
                    viewModel.onTriggerEvent(MovieDetailEvent.LoadDetail(id, "en-US"))
                }
            )
            is BaseViewState.Loading -> LoadingView(
                modifier = Modifier.absoluteOffset(x = offsetAnimation),
            )
        }
    }

    LaunchedEffect(key1 = viewModel, block = {
        viewModel.onTriggerEvent(MovieDetailEvent.LoadDetail(id, "en-US"))
    })
}

@Composable
private fun MovieDetailBody(
    title: String = "",
    pressOnBack: () -> Unit = {},
    pageContent: @Composable (PaddingValues) -> Unit
) {
    Scaffold(
        topBar = {
            SMToolbarWithNavIcon(
                title = title,
                pressOnBack = pressOnBack
            )
        },
        content = { pageContent.invoke(it) }
    )
}

@Preview(showBackground = true, name = "Light Mode")
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES, name = "Dark Mode")
@Composable
fun PokemonDetailScreenPreview() {
    SagaraMandiriTheme {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = SagaraMandiriColors.background
        ) {
            MovieDetailBody { }
        }
    }
}