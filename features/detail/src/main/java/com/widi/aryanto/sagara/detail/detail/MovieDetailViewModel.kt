package com.widi.aryanto.sagara.detail.detail

import com.widi.aryanto.sagara.mandiri.components.base.mvi.BaseViewState
import com.widi.aryanto.sagara.mandiri.components.base.mvi.MviViewModel
import com.widi.aryanto.sagara.detail.usecase.GetMovieDetail
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val getMovieDetail: GetMovieDetail
) : MviViewModel<BaseViewState<MovieDetailViewState>, MovieDetailEvent>() {

    override fun onTriggerEvent(eventType: MovieDetailEvent) {
        when (eventType) {
            is MovieDetailEvent.LoadDetail -> onLoadDetail(eventType.id, eventType.language)
        }
    }

    private fun onLoadDetail(id: Int, language: String) = safeLaunch {
        val params = GetMovieDetail.Params(id = id, language = language)
        execute(getMovieDetail(params)) { dto ->
            setState(BaseViewState.Data(MovieDetailViewState(movieDetail = dto)))
        }
    }
}