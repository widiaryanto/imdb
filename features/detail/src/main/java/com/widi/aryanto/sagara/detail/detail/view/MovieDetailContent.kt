package com.widi.aryanto.sagara.detail.detail.view

import androidx.annotation.StringRes
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.absoluteOffset
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Flag
import androidx.compose.material.icons.filled.Language
import androidx.compose.material.icons.filled.LocalMovies
import androidx.compose.material.icons.filled.Work
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.widi.aryanto.sagara.detail.data.model.dto.MovieDetailDto
import com.widi.aryanto.sagara.mandiri.common.R
import com.widi.aryanto.sagara.detail.detail.MovieDetailViewState
import com.widi.aryanto.sagara.mandiri.common.component.ExtraSmallSpacer
import com.widi.aryanto.sagara.mandiri.common.component.widget.SMDivider
import com.widi.aryanto.sagara.mandiri.common.theme.Gray400
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriShapes
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import kotlinx.coroutines.delay

@Composable
fun MovieDetailContent(
    paddingValues: PaddingValues,
    viewState: MovieDetailViewState
) {
    LazyColumn(
        contentPadding = paddingValues
    ) {
        viewState.movieDetail?.let { movieDetail ->
            item("header") {
                MovieDetailHeaderView(movieDetail = movieDetail)
            }

            item("contentInfo") {
                MovieDetailInfoView(movieDetail = movieDetail)
            }

            item("contentOverview") {
                MovieDetailOverviewView(movieDetail = movieDetail)
            }

            if (movieDetail.genres.isNotEmpty()) {
                item("contentGenres") {
                    MovieListView(
                        title = R.string.text_genres,
                        movieDetail = movieDetail,
                        key = "contentGenres"
                    )
                }
            }

            if (movieDetail.spokenLanguages.isNotEmpty()) {
                item("contentSpoken") {
                    MovieListView(
                        title = R.string.text_language,
                        movieDetail = movieDetail,
                        key = "contentSpoken"
                    )
                }
            }

            if (movieDetail.productionCompanies.isNotEmpty()) {
                item("contentProductionCompany") {
                    MovieListView(
                        title = R.string.text_production_company,
                        movieDetail = movieDetail,
                        key = "contentProductionCompany"
                    )
                }
            }

            if (movieDetail.productionCountries.isNotEmpty()) {
                item("contentProductionCompanyCountry") {
                    MovieListView(
                        title = R.string.text_production_country,
                        movieDetail = movieDetail,
                        key = "contentProductionCompanyCountry"
                    )
                }
            }

            if (movieDetail.youtube.isNotEmpty()) {
                item("contentYoutube") {
                    MovieListView(
                        title = R.string.text_youtube,
                        movieDetail = movieDetail,
                        key = "contentYoutube"
                    )
                }
            }

            if (movieDetail.reviews.isNotEmpty()) {
                item("contentReviews") {
                    MovieListView(
                        title = R.string.text_reviews,
                        movieDetail = movieDetail,
                        key = "contentReviews"
                    )
                }
            }

            item("contentOthers") {
                MovieDetailOthersView(movieDetail = movieDetail)
            }
        }
    }
}

@Composable
fun MovieListView(
    @StringRes title: Int,
    movieDetail: MovieDetailDto,
    key: String
) {
    var movieDetailGenreState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (movieDetailGenreState) 400.dp else 0.dp,
        tween(1000), label = ""
    )

    LaunchedEffect(Unit) {
        delay(400)
        movieDetailGenreState = false
    }
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .absoluteOffset(x = offsetAnimation),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(title),
            modifier = Modifier
                .padding(12.dp),
            style = SagaraMandiriTypography.titleLarge
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 8.dp, end = 8.dp),
            contentAlignment = Alignment.Center
        ) {
            Card(
                shape = SagaraMandiriShapes.medium,
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 8.dp
                )
            ) {
                when (key) {
                    "contentGenres" -> movieDetail.genres.forEach {
                        MovieListRowView(
                            name = it.name,
                            icon = Icons.Filled.LocalMovies
                        )
                    }
                    "contentSpoken" -> movieDetail.spokenLanguages.forEach {
                        MovieListRowView(
                            name = it.name,
                            icon = Icons.Filled.Language
                        )
                    }
                    "contentProductionCompany" -> movieDetail.productionCompanies.forEach {
                        MovieListRowView(
                            name = it.name,
                            icon = Icons.Filled.Work
                        )
                    }
                    "contentProductionCompanyCountry" -> movieDetail.productionCountries.forEach {
                        MovieListRowView(
                            name = it.name,
                            icon = Icons.Filled.Flag
                        )
                    }
                    "contentYoutube" -> movieDetail.youtube.forEach {
                        Box(modifier = Modifier.padding(bottom = 8.dp)) {
                            VideoPlayer(
                                videoId = it.key
                            )
                        }
                    }
                    "contentReviews" -> movieDetail.reviews.forEach {
                        MovieDetailReviewsView(
                            dto = it
                        )
                        ExtraSmallSpacer()
                        SMDivider()
                        ExtraSmallSpacer()
                    }
                }
            }
        }
    }
}

@Composable
fun MovieListRowView(
    name: String,
    icon: ImageVector
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp),
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                text = name,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                style = SagaraMandiriTypography.titleLarge
            )
            Icon(
                painter = rememberVectorPainter(icon),
                contentDescription = null,
                tint = Gray400,
                modifier = Modifier
                    .size(24.dp)
            )
        }
        ExtraSmallSpacer()
        SMDivider()
    }
}