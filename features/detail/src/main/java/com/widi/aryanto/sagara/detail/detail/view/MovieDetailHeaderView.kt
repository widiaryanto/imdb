package com.widi.aryanto.sagara.detail.detail.view

import android.content.res.Configuration
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.detail.data.model.dto.MovieDetailDto
import kotlinx.coroutines.delay

@Composable
fun MovieDetailHeaderView(
    movieDetail: MovieDetailDto?
) {
    var movieDetailHeaderState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (movieDetailHeaderState) 400.dp else 0.dp,
        tween(1000), label = ""
    )

    LaunchedEffect(Unit) {
        delay(400)
        movieDetailHeaderState = false
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(222.dp)
            .padding(top = 8.dp, bottom = 8.dp)
            .absoluteOffset(x = offsetAnimation),
        contentAlignment = Alignment.Center
    ) {
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data("https://image.tmdb.org/t/p/w500${movieDetail?.backdropPath}")
                .crossfade(true)
                .build(),
            contentDescription = null,
            contentScale = ContentScale.FillHeight,
            modifier = Modifier
                .fillMaxHeight()
                .clip(RoundedCornerShape(size = 16.dp))
        )
    }
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "Dark Mode"
)
@Composable
fun DetailHeaderItemViewPreview() {
    SagaraMandiriTheme {
        MovieDetailHeaderView(MovieDetailDto.init())
    }
}