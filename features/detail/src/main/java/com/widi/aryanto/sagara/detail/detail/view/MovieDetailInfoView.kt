package com.widi.aryanto.sagara.detail.detail.view

import android.content.res.Configuration
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.widi.aryanto.sagara.detail.data.model.dto.MovieDetailDto
import com.widi.aryanto.sagara.mandiri.common.R
import com.widi.aryanto.sagara.mandiri.common.component.widget.SMDivider
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriColors
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriShapes
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import com.widi.aryanto.sagara.mandiri.components.utils.DateUtils
import kotlinx.coroutines.delay

@Composable
fun MovieDetailInfoView(movieDetail: MovieDetailDto) {
    var movieDetailInfoState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (movieDetailInfoState) 400.dp else 0.dp,
        tween(1000), label = ""
    )

    LaunchedEffect(Unit) {
        delay(400)
        movieDetailInfoState = false
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 8.dp)
            .absoluteOffset(x = offsetAnimation),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.text_information),
            modifier = Modifier
                .padding(12.dp),
            style = SagaraMandiriTypography.titleLarge
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 8.dp, end = 8.dp),
            contentAlignment = Alignment.Center
        ) {
            Card(
                shape = SagaraMandiriShapes.medium,
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 8.dp
                )
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {
                    MovieDetailStatusView(movieDetail = movieDetail)

                    TextRow(
                        key = stringResource(id = R.string.text_title),
                        value = movieDetail.title
                    )

                    SMDivider()

                    TextRow(
                        key = stringResource(id = R.string.text_tagline),
                        value = movieDetail.tagline
                    )

                    SMDivider()

                    TextRow(
                        key = stringResource(id = R.string.text_status),
                        value = movieDetail.status
                    )

                    SMDivider()

                    TextRow(
                        key = stringResource(id = R.string.text_release_date),
                        value = DateUtils().dateMovie(movieDetail.releaseDate),
                    )
                }
            }
        }
    }
}

@Composable
private fun MovieDetailStatusView(movieDetail: MovieDetailDto) {
    Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(
                Modifier
                    .size(12.dp)
                    .clip(CircleShape)
                    .background(
                        if (movieDetail.adult) Color.Red else Color.Green
                    )
            )

            Text(
                text = if (movieDetail.adult) {
                    stringResource(id = R.string.text_adult)
                } else {
                    stringResource(id = R.string.text_not_adult)
                },
                maxLines = 1,
                overflow = TextOverflow.Visible,
                style = SagaraMandiriTypography.bodyMedium
            )
        }
    }
}

@Composable
private fun TextRow(key: String, value: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 12.dp, bottom = 12.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = key,
            maxLines = 1,
            overflow = TextOverflow.Visible,
            style = SagaraMandiriTypography.bodyMedium,
            textAlign = TextAlign.Start
        )
        Text(
            text = value,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            style = SagaraMandiriTypography.titleMedium,
            textAlign = TextAlign.End
        )
    }
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "Dark Mode"
)
@Composable
fun MovieDetailInfoViewPreview() {
    SagaraMandiriTheme {
        Surface(color = SagaraMandiriColors.background) {
            MovieDetailInfoView(
                MovieDetailDto.init()
            )
        }
    }
}