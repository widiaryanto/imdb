package com.widi.aryanto.sagara.detail.detail.view

import android.content.res.Configuration
import android.icu.text.NumberFormat
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.widi.aryanto.sagara.detail.data.model.dto.MovieDetailDto
import com.widi.aryanto.sagara.mandiri.common.R
import com.widi.aryanto.sagara.mandiri.common.component.widget.SMDivider
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriColors
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriShapes
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import java.util.Locale


@Composable
fun MovieDetailOthersView(movieDetail: MovieDetailDto) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 8.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.text_others),
            modifier = Modifier
                .padding(12.dp),
            style = SagaraMandiriTypography.titleLarge
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 8.dp, end = 8.dp),
            contentAlignment = Alignment.Center
        ) {
            Card(
                shape = SagaraMandiriShapes.medium,
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 8.dp
                )
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                ) {

                    TextRow(
                        key = stringResource(id = R.string.text_budget),
                        value = NumberFormat
                            .getCurrencyInstance(Locale("en", "US"))
                            .format(movieDetail.budget)
                    )

                    SMDivider()

                    TextRow(
                        key = stringResource(id = R.string.text_revenue),
                        value = NumberFormat
                            .getCurrencyInstance(Locale("en", "US"))
                            .format(movieDetail.revenue)
                    )

                    SMDivider()

                    TextRow(
                        key = stringResource(id = R.string.text_homepage),
                        value = movieDetail.homepage
                    )
                }
            }
        }
    }
}

@Composable
private fun TextRow(key: String, value: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 12.dp, bottom = 12.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = key,
            maxLines = 1,
            overflow = TextOverflow.Visible,
            style = SagaraMandiriTypography.bodyMedium,
            textAlign = TextAlign.Start
        )
        Text(
            text = value,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            style = SagaraMandiriTypography.titleMedium,
            textAlign = TextAlign.End
        )
    }
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "Dark Mode"
)
@Composable
fun MovieDetailOthersViewPreview() {
    SagaraMandiriTheme {
        Surface(color = SagaraMandiriColors.background) {
            MovieDetailOthersView(
                MovieDetailDto.init()
            )
        }
    }
}