package com.widi.aryanto.sagara.detail.detail.view

import android.content.res.Configuration
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.widi.aryanto.sagara.detail.data.model.dto.MovieDetailDto
import com.widi.aryanto.sagara.mandiri.common.R
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriColors
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriShapes
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import kotlinx.coroutines.delay

@Composable
fun MovieDetailOverviewView(movieDetail: MovieDetailDto) {
    var movieDetailOverviewState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (movieDetailOverviewState) 400.dp else 0.dp,
        tween(1000), label = ""
    )

    LaunchedEffect(Unit) {
        delay(400)
        movieDetailOverviewState = false
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 8.dp)
            .absoluteOffset(x = offsetAnimation),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.text_overview),
            modifier = Modifier
                .padding(12.dp),
            style = SagaraMandiriTypography.titleLarge
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 8.dp, end = 8.dp),
            contentAlignment = Alignment.Center
        ) {
            Card(
                shape = SagaraMandiriShapes.medium,
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 8.dp
                )
            ) {
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp),
                    text = movieDetail.overview,
                    overflow = TextOverflow.Ellipsis,
                    style = SagaraMandiriTypography.titleMedium
                )
            }
        }
    }
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "Dark Mode"
)
@Composable
fun MovieDetailOverviewViewPreview() {
    SagaraMandiriTheme {
        Surface(color = SagaraMandiriColors.background) {
            MovieDetailOverviewView(
                MovieDetailDto.init()
            )
        }
    }
}