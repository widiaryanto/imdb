package com.widi.aryanto.sagara.detail.detail.view

import android.content.res.Configuration
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.widi.aryanto.sagara.detail.data.model.dto.MovieDetailReviews
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import com.widi.aryanto.sagara.mandiri.components.utils.DateUtils


@Composable
fun MovieDetailReviewsView(
    dto: MovieDetailReviews,
) {
    Row {
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data("https://image.tmdb.org/t/p/w500${dto.authorDetails.avatarPath}")
                .crossfade(true)
                .build(),
            contentDescription = null,
            modifier = Modifier
                .width(100.dp)
                .padding(8.dp)
                .clip(RoundedCornerShape(size = 8.dp))
        )
        Column(
            verticalArrangement = Arrangement.spacedBy(8.dp),
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp)
        ) {
            Text(
                text = dto.author,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 2.dp),
                style = SagaraMandiriTypography.bodyMedium
            )
            Text(
                text = dto.content,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 2.dp),
                maxLines = 3,
                style = SagaraMandiriTypography.titleMedium
            )
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Text(
                    text = DateUtils().dateMovie(dto.createdAt),
                    maxLines = 1,
                    overflow = TextOverflow.Visible,
                    style = SagaraMandiriTypography.bodyMedium
                )
                Text(
                    text = dto.authorDetails.rating.toString(),
                    maxLines = 1,
                    overflow = TextOverflow.Visible,
                    style = SagaraMandiriTypography.bodyMedium
                )
            }
        }
    }
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "Dark Mode"
)
@Composable
fun MovieRowPreview() {
    SagaraMandiriTheme {
        MovieDetailReviewsView(dto = MovieDetailReviews.init())
    }
}