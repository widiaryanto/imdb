package com.widi.aryanto.sagara.detail.di

import com.widi.aryanto.sagara.detail.data.repository.MovieDetailRepository
import com.widi.aryanto.sagara.detail.usecase.GetMovieDetail
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MovieDetailModule {

    @Singleton
    @Provides
    fun provideGetMovieDetail(repository: MovieDetailRepository): GetMovieDetail {
        return GetMovieDetail(repository)
    }
}