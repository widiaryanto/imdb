package com.widi.aryanto.sagara.detail.usecase

import com.widi.aryanto.sagara.mandiri.components.network.DataState
import com.widi.aryanto.sagara.mandiri.components.network.apiCall
import com.widi.aryanto.sagara.mandiri.components.usecase.DataStateUseCase
import com.widi.aryanto.sagara.detail.data.model.dto.MovieDetailDto
import com.widi.aryanto.sagara.detail.data.model.dto.toMovieDetailDto
import com.widi.aryanto.sagara.detail.data.model.dto.toMovieReviewsDetail
import com.widi.aryanto.sagara.detail.data.model.dto.toMovieYoutubeDetail
import com.widi.aryanto.sagara.detail.data.repository.MovieDetailRepository
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

class GetMovieDetail @Inject constructor(
    private val movieRepo: MovieDetailRepository
) : DataStateUseCase<GetMovieDetail.Params, MovieDetailDto>() {

    data class Params(
        val id: Int,
        val language: String
    )

    override suspend fun FlowCollector<DataState<MovieDetailDto>>.execute(params: Params) {
        val getMovieDetail = movieRepo.getMovieDetail(id = params.id, language = params.language).toMovieDetailDto()
        val service = apiCall { getMovieDetail }
        service.map { movieDto ->
            val youtubeService = apiCall {
                movieRepo.getMovieDetailYoutube(
                    id = params.id,
                    language = params.language
                )
            }
            if (youtubeService is DataState.Success) {
                youtubeService.result.results?.toMovieYoutubeDetail()
                    ?.let { movieDto.youtube.addAll(it) }
            }
            val reviewsService = apiCall {
                movieRepo.getMovieDetailReview(
                    id = params.id,
                    language = params.language,
                    page = 1
                )
            }
            if (reviewsService is DataState.Success) {
                reviewsService.result.results?.toMovieReviewsDetail()
                    ?.let { movieDto.reviews.addAll(it) }
            }
        }
        emit(service)
    }
}