package com.widi.aryanto.sagara.mandiri.home

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.navigationBarsPadding
import com.ramcosta.composedestinations.annotation.Destination
import com.widi.aryanto.sagara.mandiri.common.provider.NavigationProvider
import com.widi.aryanto.sagara.mandiri.common.theme.RalewayFonts
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriColors
import com.widi.aryanto.sagara.mandiri.common.theme.selectedBottomItemColor
import com.widi.aryanto.sagara.mandiri.common.theme.unselectedBottomItemColor
import com.widi.aryanto.sagara.movie.list.GenreScreen
import com.widi.aryanto.sagara.settings.SettingsScreen

@Destination(start = true)
@Composable
fun HomeScreen(navigator: NavigationProvider) {
    val (currentBottomTab, setCurrentBottomTab) = rememberSaveable {
        mutableStateOf(BottomBarHomeItem.MAIN)
    }

    Crossfade(currentBottomTab, label = "") { bottomTab ->
        Scaffold(
            bottomBar = { HomeBottomNavigation(bottomTab, setCurrentBottomTab) },
            content = {
                val modifier = Modifier.padding(it)
                when (bottomTab) {
                    BottomBarHomeItem.MAIN -> GenreScreen(
                        modifier = modifier,
                        navigator = navigator
                    )
                    BottomBarHomeItem.SETTINGS -> SettingsScreen(
                        modifier = modifier,
                        navigator = navigator
                    )
                }
            }
        )
    }
}

@Composable
private fun HomeBottomNavigation(
    bottomTab: BottomBarHomeItem,
    setCurrentBottomTab: (BottomBarHomeItem) -> Unit
) {
    val pages = BottomBarHomeItem.values()

    NavigationBar(
        containerColor = SagaraMandiriColors.primary,
        modifier = Modifier.fillMaxWidth()
    ) {
        pages.forEach { page ->
            val selected = page == bottomTab
            val selectedLabelColor = if (selected) {
                selectedBottomItemColor
            } else {
                unselectedBottomItemColor
            }
            NavigationBarItem(
                icon = {
                    Icon(
                        painter = rememberVectorPainter(image = page.icon),
                        contentDescription = stringResource(page.title)
                    )
                },
                label = {
                    Text(
                        text = stringResource(page.title),
                        color = selectedLabelColor,
                        fontSize = 12.sp,
                        fontWeight = FontWeight.SemiBold,
                        fontFamily = RalewayFonts
                    )
                },
                selected = selected,
                onClick = {
                    setCurrentBottomTab.invoke(page)
                },
                colors = NavigationBarItemDefaults.colors(
                    selectedIconColor = selectedBottomItemColor,
                    unselectedIconColor = unselectedBottomItemColor,
                    indicatorColor = SagaraMandiriColors.primary
                ),
                alwaysShowLabel = true,
                modifier = Modifier.navigationBarsPadding()
            )
        }
    }
}