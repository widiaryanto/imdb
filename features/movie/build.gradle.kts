@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.com.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.parcelize)
    alias(libs.plugins.ksp)
    id("org.jetbrains.kotlin.kapt")
    id("dagger.hilt.android.plugin")
}

ksp {
    arg("compose-destinations.mode", "navgraphs")
    arg("compose-destinations.moduleName", "movie")
}

android {
    namespace = "com.widi.aryanto.sagara.movie"
    compileSdk = 33

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    implementation(project(mapOf("path" to ":core:common")))
    implementation(project(mapOf("path" to ":core:components")))
    implementation(project(mapOf("path" to ":core:compose")))
    implementation(project(mapOf("path" to ":features:detail")))

    implementation(libs.androidx.ktx)
    implementation(libs.androidx.lifecycle)
    implementation(libs.androidx.activity)
    implementation(libs.androidx.activity.compose)
    implementation(libs.appcompat)
    implementation(platform(libs.androidx.compose))
    implementation(libs.androidx.compose.ui)
    implementation(libs.androidx.compose.ui.graphics)
    implementation(libs.androidx.compose.ui.tooling.preview)
    implementation(libs.androidx.compose.material3)
    implementation(libs.systemuicontroller)
    implementation(libs.constraint.layout)
    implementation(libs.navigation)
    implementation(libs.destination.core)
    implementation(libs.destination.animation)
    ksp(libs.destination.ksp)
    implementation(libs.dagger.android)
    implementation(libs.dagger.compose)
    kapt(libs.dagger.compiler)
    implementation(libs.paging)
    implementation(libs.paging.compose)
    implementation(libs.room.ktx)
    ksp(libs.room.compiler)
    implementation(libs.moshi.kotlin)
    ksp(libs.moshi.code.gen)
    implementation(libs.moshi.lazy.adapters)
    implementation(libs.retrofit)
    implementation(libs.converter.moshi)
    implementation(libs.okhttp)
    implementation(libs.logging.interceptor)
    implementation(libs.swiperefresh)
    implementation(libs.insets)
    implementation(libs.placeholder.material)
    implementation(libs.navigation.material)
    implementation(libs.permissions)
    implementation(libs.pager)
    implementation(libs.pager.indicators)
    implementation(libs.webview)
    implementation(libs.coil)

    testImplementation(libs.junit)
    androidTestImplementation(libs.test)
    androidTestImplementation(libs.espresso)
}