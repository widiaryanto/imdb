package com.widi.aryanto.sagara.movie.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class GenresDto(
    val id: Int,
    val name: String,
) : Parcelable {
    companion object {
        fun init() = GenresDto(
            id = 1,
            name = "Action"
        )
    }
}