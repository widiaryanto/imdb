package com.widi.aryanto.sagara.movie.data.model.dto

import com.widi.aryanto.sagara.mandiri.components.extension.orZero
import com.widi.aryanto.sagara.movie.data.remote.GenreData

fun GenreData.toGenreDto() = GenresDto(
    id = id.orZero(),
    name = name.orEmpty()
)

fun List<GenreData>.toGenreDtoList() = map { it.toGenreDto() }