package com.widi.aryanto.sagara.movie.data.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDto(
    val adult: Boolean,
    val backdropPath: String,
    val genreIds: List<Int>,
    val id: Int,
    val originalLanguage: String,
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    val posterPath: String,
    val releaseDate: String,
    val title: String,
    val video: Boolean,
    val voteAverage: Double,
    val voteCount: Int
) : Parcelable {
    companion object {
        fun init() = MovieDto(
            adult = false,
            backdropPath = "/pA3vdhadJPxF5GA1uo8OPTiNQDT.jpg",
            genreIds = listOf(28,18),
            id = 678512,
            originalLanguage = "en",
            originalTitle = "Sound of Freedom",
            overview = "The story of Tim Ballard, a former US government agent, who quits his job in order to devote his life to rescuing children from global sex traffickers.",
            popularity = 3163.02,
            posterPath = "/qA5kPYZA7FkVvqcEfJRoOy4kpHg.jpg",
            releaseDate = "2023-07-03",
            title = "Sound of Freedom",
            video = false,
            voteAverage = 8.1,
            voteCount = 677
        )
    }
}