package com.widi.aryanto.sagara.movie.data.model.dto

import com.widi.aryanto.sagara.mandiri.components.extension.orFalse
import com.widi.aryanto.sagara.mandiri.components.extension.orZero
import com.widi.aryanto.sagara.movie.data.remote.MovieData

fun MovieData.toMovieDto() = MovieDto(
    adult = adult.orFalse(),
    backdropPath = backdropPath.orEmpty(),
    genreIds = genreIds.orEmpty(),
    id = id.orZero(),
    originalLanguage = originalLanguage.orEmpty(),
    originalTitle = originalTitle.orEmpty(),
    overview = overview.orEmpty(),
    popularity = popularity.orZero(),
    posterPath = posterPath.orEmpty(),
    releaseDate = releaseDate.orEmpty(),
    title = title.orEmpty(),
    video = video.orFalse(),
    voteAverage = voteAverage.orZero(),
    voteCount = voteCount.orZero()
)

fun List<MovieData>.toMovieDtoList() = map { it.toMovieDto() }