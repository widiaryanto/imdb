package com.widi.aryanto.sagara.movie.data.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GenreData(
    @Json(name = "id") val id: Int?,
    @Json(name = "name") val name: String?
)