package com.widi.aryanto.sagara.movie.data.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieResponse(
    @Json(name = "results") val results: List<MovieData>?
)