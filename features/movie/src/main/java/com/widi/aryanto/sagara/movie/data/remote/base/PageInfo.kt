package com.widi.aryanto.sagara.movie.data.remote.base

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PageInfo(
    @Json(name = "page") val page: Int?,
    @Json(name = "total_pages") val next: Int?,
    @Json(name = "total_results") val prev: Int?
)