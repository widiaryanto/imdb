package com.widi.aryanto.sagara.movie.data.remote.base

import com.serjltt.moshi.adapters.FallbackEnum
import com.squareup.moshi.Json
import com.widi.aryanto.sagara.mandiri.components.network.moshi.IValueEnum

@FallbackEnum(name = "unknown")
enum class Status(override val value: String) : IValueEnum {
    @Json(name = "Alive")
    Alive("Alive"),

    @Json(name = "Dead")
    Dead("Dead"),

    @Json(name = "unknown")
    Unknown("unknown");
}