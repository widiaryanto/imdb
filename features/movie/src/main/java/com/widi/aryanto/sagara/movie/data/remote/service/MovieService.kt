package com.widi.aryanto.sagara.movie.data.remote.service

import com.widi.aryanto.sagara.movie.data.remote.GenresResponse
import com.widi.aryanto.sagara.movie.data.remote.MovieResponse
import retrofit2.http.*

interface MovieService {
    @GET(GENRES)
    suspend fun getGenreList(
        @Query("language") language: String
    ): GenresResponse

    @GET(MOVIES)
    suspend fun getMoviesList(
        @Query("language") language: String,
        @Query("page") page: Int,
        @Query("with_genres") genre: String
    ): MovieResponse

    companion object {
        const val GENRES = "/3/genre/movie/list"
        const val MOVIES = "/3/discover/movie"
    }
}