package com.widi.aryanto.sagara.movie.data.repository

import com.widi.aryanto.sagara.movie.data.remote.service.MovieService
import javax.inject.Inject

class MovieRepository
@Inject
constructor(
    private val service: MovieService,
) {
    suspend fun getGenreList(
        language: String
    ) = service.getGenreList(language)

    suspend fun getMovieList(
        language: String,
        page: Int,
        genre: String
    ) = service.getMoviesList(language, page, genre)
}