package com.widi.aryanto.sagara.movie.di

import com.widi.aryanto.sagara.movie.data.repository.MovieRepository
import com.widi.aryanto.sagara.movie.usecase.GetGenre
import com.widi.aryanto.sagara.movie.usecase.GetMovie
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MovieModule {

    @Singleton
    @Provides
    fun provideGetGenre(repository: MovieRepository): GetGenre {
        return GetGenre(repository)
    }

    @Singleton
    @Provides
    fun provideGetMovie(repository: MovieRepository): GetMovie {
        return GetMovie(repository)
    }
}