package com.widi.aryanto.sagara.movie.list

import android.content.res.Configuration
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.absoluteOffset
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.widi.aryanto.sagara.mandiri.common.component.widget.SMToolbar
import com.widi.aryanto.sagara.mandiri.common.provider.NavigationProvider
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.mandiri.common.R
import com.widi.aryanto.sagara.mandiri.common.component.widget.EmptyView
import com.widi.aryanto.sagara.mandiri.common.component.widget.ErrorView
import com.widi.aryanto.sagara.mandiri.common.component.widget.LoadingView
import com.widi.aryanto.sagara.mandiri.components.base.mvi.BaseViewState
import com.widi.aryanto.sagara.mandiri.components.extension.cast
import com.widi.aryanto.sagara.movie.list.view.GenreContent
import kotlinx.coroutines.delay


@Composable
fun GenreScreen(
    modifier: Modifier = Modifier,
    viewModel: MovieViewModel = hiltViewModel(),
    navigator: NavigationProvider
) {
    val uiState by viewModel.uiState.collectAsState()
    var genreState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (genreState) 400.dp else 0.dp,
        tween(1000), label = ""
    )

    LaunchedEffect(Unit) {
        delay(400)
        genreState = false
    }

    GenreBody(modifier) { padding ->
        when (uiState) {
            is BaseViewState.Data -> GenreContent(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(top = 4.dp)
                    .absoluteOffset(x = offsetAnimation),
                paddingValues = padding,
                viewState = uiState.cast<BaseViewState.Data<MovieState>>().value,
                selectItem = { data -> navigator.openMovies(
                    id = data.id,
                    title = data.name
                ) }
            )
            is BaseViewState.Empty -> EmptyView(
                modifier = Modifier
                    .fillMaxSize()
                    .absoluteOffset(x = offsetAnimation)
            )
            is BaseViewState.Error -> ErrorView(
                modifier = Modifier.absoluteOffset(x = offsetAnimation),
                e = uiState.cast<BaseViewState.Error>().throwable,
                action = {
                    viewModel.onTriggerEvent(MovieEvent.LoadGenre("en"))
                }
            )
            is BaseViewState.Loading -> LoadingView(
                modifier = Modifier.absoluteOffset(x = offsetAnimation),
            )
        }

        LaunchedEffect(key1 = viewModel, block = {
            viewModel.onTriggerEvent(MovieEvent.LoadGenre("en"))
        })
    }
}

@Composable
private fun GenreBody(
    modifier: Modifier = Modifier,
    pageContent: @Composable (PaddingValues) -> Unit
) {
    Scaffold(
        modifier = modifier,
        topBar = { SMToolbar(R.string.toolbar_genre_title) },
        content = { pageContent.invoke(it) }
    )
}

@Preview(showBackground = true, name = "Light Mode")
@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES, name = "Dark Mode")
@Composable
fun GenreScreenPreview() {
    SagaraMandiriTheme {
        Surface {}
    }
}