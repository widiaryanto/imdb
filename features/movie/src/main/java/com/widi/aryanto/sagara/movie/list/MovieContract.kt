package com.widi.aryanto.sagara.movie.list

import androidx.paging.PagingData
import com.widi.aryanto.sagara.mandiri.components.network.DataState
import com.widi.aryanto.sagara.movie.data.model.dto.GenresDto
import com.widi.aryanto.sagara.movie.data.model.dto.MovieDto
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow

data class MovieState(
    val movieData: Flow<PagingData<MovieDto>> = emptyFlow(),
    val genreList: Flow<DataState<List<GenresDto>>> = emptyFlow()
)

sealed class MovieEvent {
    data class LoadGenre(val language: String) : MovieEvent()
    data class LoadMovie(val language: String, val genre: String) : MovieEvent()
}