package com.widi.aryanto.sagara.movie.list

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.widi.aryanto.sagara.mandiri.components.base.mvi.BaseViewState
import com.widi.aryanto.sagara.mandiri.components.base.mvi.MviViewModel
import com.widi.aryanto.sagara.movie.usecase.GetGenre
import com.widi.aryanto.sagara.movie.usecase.GetMovie
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val getGenre: GetGenre,
    private val getMovie: GetMovie
) : MviViewModel<BaseViewState<MovieState>, MovieEvent>() {
    private val config = PagingConfig(pageSize = 20)

    override fun onTriggerEvent(eventType: MovieEvent) {
        when (eventType) {
            is MovieEvent.LoadGenre -> onLoadGenre(eventType.language)
            is MovieEvent.LoadMovie -> onLoadMovie(eventType.language, eventType.genre)
        }
    }

    private fun onLoadGenre(language: String) = safeLaunch {
        setState(BaseViewState.Loading)
        val params = GetGenre.Params(language)
        val genreFlow = getGenre(params)
        setState(BaseViewState.Data(MovieState(genreList = genreFlow)))
    }

    private fun onLoadMovie(language: String, genre: String) = safeLaunch {
        setState(BaseViewState.Loading)
        val params = GetMovie.Params(config, language, genre)
        val pagedFlow = getMovie(params).cachedIn(scope = viewModelScope)
        setState(BaseViewState.Data(MovieState(movieData = pagedFlow)))
    }
}