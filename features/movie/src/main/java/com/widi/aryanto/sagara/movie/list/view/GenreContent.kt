package com.widi.aryanto.sagara.movie.list.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import com.widi.aryanto.sagara.mandiri.components.network.DataState
import com.widi.aryanto.sagara.movie.data.model.dto.GenresDto
import com.widi.aryanto.sagara.mandiri.compose.rememberFlowWithLifecycle
import com.widi.aryanto.sagara.movie.list.MovieState
import kotlinx.coroutines.flow.collectLatest

@Composable
fun GenreContent(
    modifier: Modifier,
    paddingValues: PaddingValues,
    viewState: MovieState,
    selectItem: (GenresDto) -> Unit = {}
) {
    val genreItems = rememberFlowWithLifecycle(viewState.genreList)
    val data = remember { mutableStateListOf<GenresDto>()}
    LaunchedEffect(key1 = Unit, block = {
        genreItems.collectLatest {
            when (it) {
                is DataState.Success -> { data.addAll(it.result) }
                is DataState.Error -> { data.clear() }
            }
        }
    })

    LazyColumn(
        contentPadding = paddingValues,
        modifier = modifier
    ) {
        items(data.size) { index ->
            data[index].let {
                GenreRow(
                    modifier = modifier,
                    dto = it,
                    onDetailClick = { selectItem.invoke(it) }
                )
            }
        }
    }
}