package com.widi.aryanto.sagara.movie.list.view

import android.content.res.Configuration
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriColors
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import com.widi.aryanto.sagara.movie.data.model.dto.GenresDto

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GenreRow(
    modifier: Modifier,
    dto: GenresDto,
    onDetailClick: () -> Unit = {}
) {
    Card(
        onClick = onDetailClick,
        modifier = modifier
            .fillMaxWidth()
            .height(80.dp)
            .padding(
                vertical = 4.dp,
                horizontal = 8.dp
            ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 8.dp
        )
    ) {
        Column(
            verticalArrangement = Arrangement.spacedBy(8.dp),
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 12.dp, start = 8.dp, bottom = 4.dp)
        ) {
            Text(
                text = dto.name,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 8.dp, bottom = 8.dp),
                style = SagaraMandiriTypography.displaySmall
            )
        }
    }
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "Dark Mode"
)
@Composable
fun GenreRowPreview() {
    SagaraMandiriTheme {
        Surface(color = SagaraMandiriColors.background) {
            GenreRow(modifier = Modifier, dto = GenresDto.init())
        }
    }
}