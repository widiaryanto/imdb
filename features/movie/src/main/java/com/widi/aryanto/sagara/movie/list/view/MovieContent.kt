package com.widi.aryanto.sagara.movie.list.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.SwipeRefreshIndicator
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.widi.aryanto.sagara.mandiri.compose.rememberFlowWithLifecycle
import com.widi.aryanto.sagara.movie.data.model.dto.MovieDto
import com.widi.aryanto.sagara.movie.list.MovieState

@Composable
fun MovieContent(
    modifier: Modifier,
    paddingValues: PaddingValues,
    viewState: MovieState,
    selectItem: (MovieDto) -> Unit = {}
) {
    val pagingItems = rememberFlowWithLifecycle(viewState.movieData).collectAsLazyPagingItems()

    SwipeRefresh(
        state = rememberSwipeRefreshState(
            isRefreshing = pagingItems.loadState.refresh == LoadState.Loading
        ),
        onRefresh = { pagingItems.refresh() },
        indicatorPadding = paddingValues,
        indicator = { state, trigger ->
            SwipeRefreshIndicator(
                state = state,
                refreshTriggerDistance = trigger,
                scale = true
            )
        },
        content = {
            LazyColumn(
                contentPadding = paddingValues,
                modifier = modifier,
            ) {
                items(pagingItems.itemCount) { index ->
                    pagingItems[index]?.let {
                        MovieRow(
                            dto = it,
                            onDetailClick = { selectItem.invoke(it) }
                        )
                    }
                }

                if (pagingItems.loadState.append == LoadState.Loading) {
                    item {
                        Box(
                            Modifier
                                .fillMaxWidth()
                                .padding(24.dp)
                        ) {
                            CircularProgressIndicator(Modifier.align(Alignment.Center))
                        }
                    }
                }
            }
        }
    )
}