package com.widi.aryanto.sagara.movie.list.view

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import com.widi.aryanto.sagara.mandiri.components.utils.DateUtils
import com.widi.aryanto.sagara.movie.data.model.dto.MovieDto


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieRow(
    dto: MovieDto,
    onDetailClick: () -> Unit = {}
) {

    Card(
        onClick = onDetailClick,
        modifier = Modifier
            .fillMaxWidth()
            .height(150.dp)
            .padding(
                vertical = 4.dp,
                horizontal = 8.dp
            ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 8.dp
        )
    ) {
        Row {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data("https://image.tmdb.org/t/p/w500${dto.posterPath}")
                    .crossfade(true)
                    .build(),
                contentDescription = null,
                modifier = Modifier
                    .padding(8.dp)
                    .clip(RoundedCornerShape(size = 8.dp))
            )
            Column(
                verticalArrangement = Arrangement.spacedBy(8.dp),
                modifier = Modifier
                    .fillMaxSize()
                    .padding(top = 12.dp, start = 4.dp, bottom = 4.dp)
            ) {
                Text(
                    text = dto.title,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 2.dp),
                    style = SagaraMandiriTypography.bodyMedium
                )
                Text(
                    text = dto.overview,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 2.dp),
                    maxLines = 3,
                    style = SagaraMandiriTypography.titleMedium
                )
                Box {
                    Row(
                        horizontalArrangement = Arrangement.spacedBy(8.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Spacer(
                            Modifier
                                .size(12.dp)
                                .clip(CircleShape)
                                .background(if (dto.adult) Color.Red else Color.Green)
                        )
                        Text(
                            text = DateUtils().dateMovie(dto.releaseDate),
                            maxLines = 1,
                            overflow = TextOverflow.Visible,
                            style = SagaraMandiriTypography.bodyMedium
                        )
                    }
                }
            }
        }
    }
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Preview(
    showBackground = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
    name = "Dark Mode"
)
@Composable
fun MovieRowPreview() {
    SagaraMandiriTheme {
        MovieRow(dto = MovieDto.init())
    }
}