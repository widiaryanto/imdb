package com.widi.aryanto.sagara.movie.usecase

import com.widi.aryanto.sagara.mandiri.components.network.DataState
import com.widi.aryanto.sagara.mandiri.components.network.apiCall
import com.widi.aryanto.sagara.mandiri.components.usecase.DataStateUseCase
import com.widi.aryanto.sagara.movie.data.model.dto.GenresDto
import com.widi.aryanto.sagara.movie.data.model.dto.toGenreDtoList
import com.widi.aryanto.sagara.movie.data.repository.MovieRepository
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

class GetGenre @Inject constructor(
    private val repository: MovieRepository
) : DataStateUseCase<GetGenre.Params, List<GenresDto>>() {

    data class Params(
        val language: String
    )

    override suspend fun FlowCollector<DataState<List<GenresDto>>>.execute(params: Params) {
        val getGenre = repository.getGenreList(params.language).results.toGenreDtoList()
        val service = apiCall { getGenre }
        emit(service)
    }
}