package com.widi.aryanto.sagara.movie.usecase

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.widi.aryanto.sagara.mandiri.components.usecase.FlowPagingUseCase
import com.widi.aryanto.sagara.movie.data.model.dto.MovieDto
import com.widi.aryanto.sagara.movie.data.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovie @Inject constructor(
    private val repository: MovieRepository
) : FlowPagingUseCase<GetMovie.Params, MovieDto>() {

    data class Params(
        val pagingConfig: PagingConfig,
        val language: String,
        val genre: String
    )

    override fun execute(params: Params): Flow<PagingData<MovieDto>> {
        return Pager(
            config = params.pagingConfig,
            pagingSourceFactory = { MoviePagingSource(repository, params.language, params.genre) }
        ).flow
    }
}