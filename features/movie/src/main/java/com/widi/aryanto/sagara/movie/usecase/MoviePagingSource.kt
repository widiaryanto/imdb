package com.widi.aryanto.sagara.movie.usecase

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.widi.aryanto.sagara.movie.data.model.dto.MovieDto
import com.widi.aryanto.sagara.movie.data.model.dto.toMovieDtoList
import com.widi.aryanto.sagara.movie.data.repository.MovieRepository
import java.io.IOException

class MoviePagingSource(
    private val repository: MovieRepository,
    private val language: String,
    private val genre: String
) : PagingSource<Int, MovieDto>() {
    override fun getRefreshKey(state: PagingState<Int, MovieDto>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MovieDto> {
        val page = params.key ?: 1
        return try {
            val response = repository.getMovieList(language, page, genre)
            val movieList = response.results.orEmpty().toMovieDtoList()
            LoadResult.Page(
                data = movieList,
                prevKey = if (page == 1) null else page - 1,
                nextKey = if (movieList.isEmpty()) null else page + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        }
    }
}