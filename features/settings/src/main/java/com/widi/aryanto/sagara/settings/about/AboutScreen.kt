package com.widi.aryanto.sagara.settings.about

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.ramcosta.composedestinations.annotation.Destination
import com.widi.aryanto.sagara.mandiri.common.R
import com.widi.aryanto.sagara.mandiri.common.component.ExtraSmallSpacer
import com.widi.aryanto.sagara.mandiri.common.component.MediumSpacer
import com.widi.aryanto.sagara.mandiri.common.component.SmallSpacer
import com.widi.aryanto.sagara.mandiri.common.component.widget.SMToolbarWithNavIcon
import com.widi.aryanto.sagara.mandiri.common.provider.NavigationProvider
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriColors
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriShapes
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTheme
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import kotlinx.coroutines.delay

@Destination
@Composable
fun AboutScreen(
    navigator: NavigationProvider
) {
    val uriHandler = LocalUriHandler.current
    val linkedinLink = "https://linkedin.com/in/widiaryanto"
    val gitlabLink = "https://gitlab.com/widiaryanto"
    var aboutState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (aboutState) 400.dp else 0.dp,
        tween(1000), label = ""
    )

    LaunchedEffect(Unit) {
        delay(200)
        aboutState = false
    }
    Scaffold(
        topBar = {
            SMToolbarWithNavIcon(
                stringResource(id = R.string.toolbar_about_title),
                pressOnBack = {
                    navigator.navigateUp()
                }
            )
        },
        content = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(it),
                contentAlignment = Alignment.Center
            ) {
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                        .absoluteOffset(x = offsetAnimation),
                    shape = SagaraMandiriShapes.medium,
                    elevation = CardDefaults.cardElevation(
                        defaultElevation = 8.dp
                    )
                ) {
                    Column(
                        modifier = Modifier
                            .wrapContentHeight()
                            .padding(12.dp),
                        verticalArrangement = Arrangement.Top,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_profile),
                            contentDescription = null,
                            modifier = Modifier
                                .size(140.dp)
                                .clip(CircleShape)
                        )
                        MediumSpacer()
                        Text(
                            text = "Muhamad Widi Aryanto",
                            style = SagaraMandiriTypography.displaySmall,
                            textAlign = TextAlign.Center
                        )
                        ExtraSmallSpacer()
                        Text(
                            text = "Android Developer",
                            style = SagaraMandiriTypography.headlineMedium,
                            textAlign = TextAlign.Center
                        )
                        ExtraSmallSpacer()
                        Text(
                            text = "Developed with concept of Modular Framework, Clean Architecture, and Jetpack Compose",
                            style = SagaraMandiriTypography.headlineMedium,
                            textAlign = TextAlign.Center
                        )
                        SmallSpacer()
                        ClickableText(
                            text = AnnotatedString(text = gitlabLink),
                            style = SagaraMandiriTypography.titleLarge,
                            onClick = {
                                uriHandler.openUri(gitlabLink)
                            }
                        )
                        SmallSpacer()
                        ClickableText(
                            text = AnnotatedString(text = linkedinLink),
                            style = SagaraMandiriTypography.titleLarge,
                            onClick = {
                                uriHandler.openUri(linkedinLink)
                            }
                        )
                    }
                }
            }
        }
    )
}

@Preview(
    showBackground = true,
    name = "Light Mode"
)
@Composable
fun AboutScreenPreview() {
    SagaraMandiriTheme {
        Surface(modifier = Modifier.background(SagaraMandiriColors.background)) {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(12.dp),
                shape = SagaraMandiriShapes.medium,
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 8.dp
                )
            ) {
                Column(
                    modifier = Modifier
                        .wrapContentHeight()
                        .padding(12.dp),
                    verticalArrangement = Arrangement.Top,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_profile),
                        contentDescription = null,
                        modifier = Modifier
                            .size(140.dp)
                            .clip(CircleShape)
                    )
                    MediumSpacer()
                    Text(
                        text = "Muhamad Widi Aryanto",
                        style = SagaraMandiriTypography.displaySmall,
                        textAlign = TextAlign.Center
                    )
                    ExtraSmallSpacer()
                    Text(
                        text = "Android Developer",
                        style = SagaraMandiriTypography.headlineMedium,
                        textAlign = TextAlign.Center
                    )
                    ExtraSmallSpacer()
                    Text(
                        text = "Developed with concept of Modular Framework, Clean Architecture, and Jetpack Compose",
                        style = SagaraMandiriTypography.headlineMedium,
                        textAlign = TextAlign.Center
                    )
                    SmallSpacer()
                    ClickableText(
                        text = AnnotatedString(text = "https://gitlab.com/widiaryanto"),
                        style = SagaraMandiriTypography.titleLarge,
                        onClick = {
                        }
                    )
                    SmallSpacer()
                    ClickableText(
                        text = AnnotatedString(text = "https://linkedin.com/in/widiaryanto"),
                        style = SagaraMandiriTypography.titleLarge,
                        onClick = {
                        }
                    )
                }
            }
        }
    }
}