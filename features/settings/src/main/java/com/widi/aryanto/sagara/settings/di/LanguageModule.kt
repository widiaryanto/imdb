package com.widi.aryanto.sagara.settings.di

import com.widi.aryanto.sagara.settings.data.repository.LanguageRepository
import com.widi.aryanto.sagara.settings.usecase.GetLanguage
import com.widi.aryanto.sagara.settings.usecase.SaveLanguage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LanguageModule {

    @Singleton
    @Provides
    fun provideSaveLanguage(repository: LanguageRepository): SaveLanguage {
        return SaveLanguage(repository)
    }

    @Singleton
    @Provides
    fun provideGetLanguage(repository: LanguageRepository): GetLanguage {
        return GetLanguage(repository)
    }
}