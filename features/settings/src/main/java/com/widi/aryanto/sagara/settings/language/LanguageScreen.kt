package com.widi.aryanto.sagara.settings.language

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.widi.aryanto.sagara.mandiri.common.component.widget.SMToolbarWithNavIcon
import com.widi.aryanto.sagara.mandiri.common.provider.NavigationProvider
import com.widi.aryanto.sagara.mandiri.compose.SetLanguage
import com.widi.aryanto.sagara.mandiri.common.R
import com.widi.aryanto.sagara.mandiri.common.theme.Red700
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriShapes
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import com.widi.aryanto.sagara.mandiri.compose.clickableSingle
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Destination
@Composable
fun LanguageScreen(
    viewModel: LanguageViewModel = hiltViewModel(),
    navigator: NavigationProvider
) {
    var langs by remember { viewModel.langs }
    val scope = rememberCoroutineScope()
    var languageState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (languageState) 400.dp else 0.dp,
        tween(1000), label = ""
    )

    LaunchedEffect(Unit) {
        delay(200)
        languageState = false
    }

    SetLanguage(langs.find { it.isSelected }?.code.toString())

    Scaffold(
        topBar = {
            SMToolbarWithNavIcon(
                stringResource(id = R.string.toolbar_app_language_title),
                pressOnBack = {
                    navigator.navigateUp()
                }
            )
        },
        content = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(it),
                contentAlignment = Alignment.Center
            ) {
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(12.dp)
                        .absoluteOffset(x = offsetAnimation),
                    shape = SagaraMandiriShapes.medium,
                    elevation = CardDefaults.cardElevation(
                        defaultElevation = 8.dp
                    )
                ) {
                    LazyColumn {
                        items(items = langs) { lang ->
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(50.dp)
                                    .clickableSingle {
                                        scope.launch {
                                            viewModel.saveLanguageCode(lang.code)
                                        }
                                        langs = langs.map { dto ->
                                            if (lang.id == dto.id) {
                                                dto.copy(isSelected = true)
                                            } else {
                                                dto.copy(isSelected = false)
                                            }
                                        }
                                    }
                                    .padding(16.dp),
                                horizontalArrangement = Arrangement.SpaceBetween,
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(text = lang.name, style = SagaraMandiriTypography.bodyMedium)
                                AnimatedVisibility(lang.isSelected) {
                                    Icon(
                                        imageVector = Icons.Default.Check,
                                        contentDescription = "Selected",
                                        tint = Red700,
                                        modifier = Modifier.size(20.dp)
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    )
}