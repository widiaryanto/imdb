package com.widi.aryanto.sagara.settings.language

import androidx.compose.runtime.mutableStateOf
import com.widi.aryanto.sagara.mandiri.common.provider.LanguageProvider
import com.widi.aryanto.sagara.mandiri.common.provider.ResourceProvider
import com.widi.aryanto.sagara.mandiri.components.base.mvvm.MvvmViewModel
import com.widi.aryanto.sagara.mandiri.common.R
import com.widi.aryanto.sagara.settings.model.dto.LanguageDto
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LanguageViewModel @Inject constructor(
    private val languageProvider: LanguageProvider,
    private val resourceProvider: ResourceProvider
) : MvvmViewModel() {

    private var languages = emptyList<LanguageDto>()

    val langs = mutableStateOf<List<LanguageDto>>(emptyList())

    init {
        languages = getLanguages()
        getLanguage()
    }

    fun saveLanguageCode(code: String) {
        languageProvider.saveLanguageCode(code)
    }

    private fun getLanguageCode(): String {
        return languageProvider.getLanguageCode()
    }

    private fun getLanguage() {
        languages.map {
            it.isSelected = it.code == getLanguageCode()
        }
        langs.value = languages
    }

    private fun getLanguages(): List<LanguageDto> {
        return listOf(
            LanguageDto(
                id = 1,
                code = "en",
                name = resourceProvider.getString(R.string.text_language_english),
                isSelected = false
            ),
            LanguageDto(
                id = 2,
                code = "in",
                name = resourceProvider.getString(R.string.text_language_indonesia),
                isSelected = false
            )
        )
    }
}