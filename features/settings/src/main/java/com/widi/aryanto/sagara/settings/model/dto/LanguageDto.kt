package com.widi.aryanto.sagara.settings.model.dto

data class LanguageDto(
    val id: Int,
    val code: String,
    val name: String,
    var isSelected: Boolean = false
)