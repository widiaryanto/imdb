package com.widi.aryanto.sagara.settings.usecase

import com.widi.aryanto.sagara.settings.data.repository.LanguageRepository
import com.widi.aryanto.sagara.mandiri.components.usecase.ReturnUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetLanguage @Inject constructor(
    private val repository: LanguageRepository
) : ReturnUseCase<Unit, String>() {

    override suspend fun execute(params: Unit): Flow<String> {
        return repository.getLanguage
    }
}