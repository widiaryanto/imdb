package com.widi.aryanto.sagara.settings.usecase

import com.widi.aryanto.sagara.settings.data.repository.LanguageRepository
import com.widi.aryanto.sagara.mandiri.components.usecase.LocalUseCase
import kotlinx.coroutines.flow.FlowCollector
import javax.inject.Inject

class SaveLanguage @Inject constructor(
    private val repository: LanguageRepository
) : LocalUseCase<SaveLanguage.Params, Unit>() {
    data class Params(
        val language: String
    )

    override suspend fun FlowCollector<Unit>.execute(params: Params) {
        repository.setLanguage(params.language)
        emit(Unit)
    }
}