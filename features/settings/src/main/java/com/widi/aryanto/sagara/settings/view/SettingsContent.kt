package com.widi.aryanto.sagara.settings.view

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.hilt.navigation.compose.hiltViewModel
import com.widi.aryanto.sagara.mandiri.common.R
import com.widi.aryanto.sagara.mandiri.common.component.widget.SMDivider
import com.widi.aryanto.sagara.mandiri.common.component.widget.ThemeSwitch
import com.widi.aryanto.sagara.mandiri.common.provider.NavigationProvider
import com.widi.aryanto.sagara.mandiri.common.theme.Gray400
import com.widi.aryanto.sagara.mandiri.common.theme.SagaraMandiriTypography
import com.widi.aryanto.sagara.mandiri.compose.clickableSingle
import com.widi.aryanto.sagara.settings.SettingsViewModel
import kotlinx.coroutines.delay

@Composable
fun SettingsContent(
    paddingValues: PaddingValues,
    viewModel: SettingsViewModel = hiltViewModel(),
    checkedState: MutableState<Boolean>,
    navigator: NavigationProvider? = null
) {
    val version = "1.0.0"
    var cardState by remember { mutableStateOf(true) }
    val offsetAnimation: Dp by animateDpAsState(
        if (cardState) 400.dp else 0.dp,
        tween(1000), label = ""
    )

    LaunchedEffect(Unit) {
        delay(200)
        cardState = false
    }
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingValues),
        contentAlignment = Alignment.TopStart
    ) {
        Card(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
                .wrapContentHeight()
                .absoluteOffset(x = offsetAnimation)
        ) {
            ConstraintLayout(
                modifier = Modifier
                    .padding(16.dp)
            ) {
                val (
                    lblThemeMode, switchThemeMode,
                    viewDivider1,
                    lblAppLanguage,
                    viewDivider2,
                    lblAbout,
                    viewDivider3,
                    lblAppVersion, tvAppVersion
                ) = createRefs()

                Text(
                    text = stringResource(id = R.string.text_theme_mode),
                    style = SagaraMandiriTypography.bodyMedium,
                    modifier = Modifier.constrainAs(lblThemeMode) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                    }
                )

                AndroidView(
                    factory = { context ->
                        ThemeSwitch(context).apply {
                            isChecked = checkedState.value
                            setOnCheckedChangeListener { _, isChecked ->
                                checkedState.value = isChecked
                                viewModel.saveThemeMode(isChecked)
                            }
                        }
                    },
                    modifier = Modifier.constrainAs(switchThemeMode) {
                        top.linkTo(lblThemeMode.top)
                        bottom.linkTo(lblThemeMode.bottom)
                        end.linkTo(parent.end)
                    }
                )

                SMDivider(
                    modifier = Modifier
                        .padding(top = 12.dp, bottom = 12.dp)
                        .constrainAs(viewDivider1) {
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                            top.linkTo(lblThemeMode.bottom)
                            height = Dimension.fillToConstraints
                        }
                )

                Row(
                    modifier = Modifier
                        .constrainAs(lblAppLanguage) {
                            top.linkTo(viewDivider1.bottom)
                            start.linkTo(parent.start)
                        }
                        .clickableSingle {
                            navigator?.openAppLanguage()
                        }
                ) {
                    Text(
                        text = stringResource(id = R.string.text_app_language),
                        style = SagaraMandiriTypography.bodyMedium,
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f)
                            .align(Alignment.CenterVertically)
                    )

                    Icon(
                        painter = rememberVectorPainter(Icons.Default.KeyboardArrowRight),
                        contentDescription = null,
                        tint = Gray400,
                        modifier = Modifier.wrapContentSize()
                    )
                }

                SMDivider(
                    modifier = Modifier
                        .padding(top = 12.dp, bottom = 12.dp)
                        .constrainAs(viewDivider2) {
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                            top.linkTo(lblAppLanguage.bottom)
                            height = Dimension.fillToConstraints
                        }
                )

                Row(
                    modifier = Modifier
                        .constrainAs(lblAbout) {
                            top.linkTo(viewDivider2.bottom)
                            start.linkTo(parent.start)
                        }
                        .clickableSingle {
                            navigator?.openAbout()
                        }
                ) {
                    Text(
                        text = stringResource(id = R.string.text_about),
                        style = SagaraMandiriTypography.bodyMedium,
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f)
                            .align(Alignment.CenterVertically)
                    )

                    Icon(
                        painter = rememberVectorPainter(Icons.Default.KeyboardArrowRight),
                        contentDescription = null,
                        tint = Gray400,
                        modifier = Modifier.wrapContentSize()
                    )
                }

                SMDivider(
                    modifier = Modifier
                        .padding(top = 12.dp, bottom = 12.dp)
                        .constrainAs(viewDivider3) {
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                            top.linkTo(lblAbout.bottom)
                            height = Dimension.fillToConstraints
                        }
                )

                Text(
                    text = stringResource(id = R.string.text_app_version),
                    style = SagaraMandiriTypography.bodyMedium,
                    modifier = Modifier.constrainAs(lblAppVersion) {
                        top.linkTo(viewDivider3.bottom)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                    }
                )

                Text(
                    text = version,
                    style = SagaraMandiriTypography.titleMedium,
                    modifier = Modifier.constrainAs(tvAppVersion) {
                        top.linkTo(lblAppVersion.top)
                        bottom.linkTo(lblAppVersion.bottom)
                        end.linkTo(parent.end)
                    }
                )
            }
        }
    }
}