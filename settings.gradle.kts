pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "Sagara Mandiri"
include(":app")
include(":core:common")
include(":core:compose")
include(":core:components")
include(":features:splash")
include(":features:home")
include(":features:settings")
include(":features:movie")
include(":features:detail")
